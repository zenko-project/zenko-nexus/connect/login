use axum::{
    extract::{Query, State},
    Form,
};
use std::sync::Arc;
use tracing::debug;

use crate::{app::state::AppState, dtos};

pub async fn handle(
    State(state): State<Arc<AppState>>,
    Query(query): Query<dtos::index::Query>,
    Form(body): Form<dtos::index::Body>,
) -> Result<(), ()> {
    debug!("Query: {:?}", query);
    debug!("Body: {:?}", body);

    let mut cache = match state.redis_client.get_connection() {
        Ok(connection) => connection,
        Err(_) => return Err(()),
    };

    Ok(())
}
