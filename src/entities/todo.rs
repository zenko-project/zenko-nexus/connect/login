use serde::{Deserialize, Serialize};

use super::Entity;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "_id")]
    pub id: String,
}

impl Entity for User {
    fn id(self) -> String {
        self.id
    }
}
